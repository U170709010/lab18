public class MatrixCalculator {

    public static void main(String[] args) {
        
         int[][] matrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};

         int[][] matrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};

         int[][] result=addition(matrixA,matrixB);

         String line="";

         for (int i=0;i<3;i++){

             line="";

			 for (int x=0;x<3;x++){

                 line+=result[i][x]+"   ";

             }

             System.out.println(line);
         }
    }
    public static int[][] addition(int[][] x1,int[][] x2) {

         int[][] k ={{0,0,0},{0,0,0},{0,0,0}};

		 for(int i=0;i<3;i++){

            for(int x=0;x<3;x++){

                k[i][x]=x1[i][x]+x2[i][x];

            }
         }
         return k;
   }
}

